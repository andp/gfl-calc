export function localSet(key, val) {
  if (!val) return null;
  if (typeof window === 'undefined') return null;
  try {
    return window.localStorage.setItem(key, JSON.stringify(val));
  } catch (ex) {
    return null;
  }
}
export function localGet(key) {
  const data = {};
  if (typeof window === 'undefined') return data;
  try {
    const windowData = JSON.parse(window.localStorage.getItem(key));
    if (windowData) {
      Object.assign(data, windowData);
    }
    return data;
  } catch (ex) {
    return data;
  }
}
