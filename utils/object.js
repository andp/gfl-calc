export default function isObjectEmpty(obj) {
  return !!obj && obj.constructor === Object && Object.keys(obj).length === 0;
}
