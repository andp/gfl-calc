export default function ExpTable({
  initialLevel,
  desiredLevel,
  isMvp = false,
  isLeader = false,
  initialExpAmount,
  desiredExpAmount,
  expDifference,
  amountOfLevels,
  coreBonus,
  expPerBattle,
  totalBattles,
  displayName,
  totalExpPerRun,
  runs,
  amountOfCombatReports
}) {
  return (
    <table id="#results" className="table table-bordered table-hover">
      <tbody>
        <tr>
          <td>Initial Level:</td>
          <td>{initialLevel}</td>
        </tr>
        <tr>
          <td>Desired Level:</td>
          <td>{desiredLevel}</td>
        </tr>
        <tr>
          <td>Amount of Combat Reports:</td>
          <td>{amountOfCombatReports}</td>
        </tr>
        <tr>
          <td>Is MVP:</td>
          <td>{isMvp ? 'yes' : 'no'}</td>
        </tr>
        <tr>
          <td>Is Leader:</td>
          <td>{isLeader ? 'yes' : 'no'}</td>
        </tr>
        <tr>
          <td>Initial Amount of EXP:</td>
          <td>{initialExpAmount}</td>
        </tr>
        <tr>
          <td>Desired Amount of EXP:</td>
          <td>{desiredExpAmount}</td>
        </tr>
        <tr>
          <td>EXP Difference:</td>
          <td>{expDifference}</td>
        </tr>
        <tr>
          <td>Amount of Levels:</td>
          <td>{amountOfLevels}</td>
        </tr>
        <tr>
          <td>Dummy Link Bonus:</td>
          <td>{coreBonus}</td>
        </tr>
        <tr>
          <td>Exp Per Battle:</td>
          <td>{expPerBattle}</td>
        </tr>
        <tr>
          <td>Total Battles:</td>
          <td>{totalBattles}</td>
        </tr>
        <tr>
          <td>Total Exp Per {displayName} Run:</td>
          <td>{totalExpPerRun}</td>
        </tr>
        <tr className="table-primary">
          <td>Total Runs Needed:</td>
          <td>{runs}</td>
        </tr>
      </tbody>
    </table>
  );
}
