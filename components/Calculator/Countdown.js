import React, { useState, useEffect } from 'react';

export default function Calculator(props) {
  const [count, setCount] = useState(props.number);
  useEffect(() => setCount(props.number), [props.number]);
  return (
    <div className="btn-group" role="group" aria-label="Countdown Button" id="countdown">
      <button
        type="button"
        id="runComplete"
        className="btn btn-primary"
        onClick={() => setCount(count - 1)}
      >
        Run Complete
      </button>
      <button type="button" id="runCounter" className="btn btn-secondary" disabled>
        {count}
      </button>
    </div>
  );
}
