const TableHeader = ({ headerText, columnNames = ['Manpower', 'Ammo', 'Rations', 'Parts'] }) => (
  <thead>
    <tr>
      {headerText && <th scope="col">{headerText}</th>}
      {columnNames.map(columnName => (
        <th scope="col" key={columnName}>
          {columnName}
        </th>
      ))}
    </tr>
  </thead>
);

export default TableHeader;
