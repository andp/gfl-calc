import React, { Fragment, useState } from "react";

const Table = ({ headerText, subtext, children }) => {
  const [isCollapsed, setIsCollapsed] = useState();
  return (
    <Fragment>
      {headerText && (
        <h1>
          {headerText}
          <button
            className="btn btn-primary"
            type="button"
            onClick={() => setIsCollapsed(!isCollapsed)}
          >
            {isCollapsed ? "+" : "-"}
          </button>
        </h1>
      )}

      {!isCollapsed && (
        <Fragment>
          {subtext && <p>{subtext}</p>}
          <table className="table table-bordered table-hover">{children}</table>
        </Fragment>
      )}
    </Fragment>
  );
};

export default Table;
