import Link from 'next/link';

const Header = () => (
  <ul className="nav nav-tabs">
    <li className="nav-item">
      <Link href="/Calculator" as="/">
        <a className="nav-link">Calculator</a>
      </Link>
    </li>
    <li className="nav-item">
      <Link href="/s" as="/s">
        <a className="nav-link">Skills</a>
      </Link>
    </li>
    <li className="nav-item">
      <Link href="/t" as="/t">
        <a className="nav-link">T-Doll Crafting</a>
      </Link>
    </li>
    <li className="nav-item">
      <Link href="/e" as="/e">
        <a className="nav-link">Equipment Crafting</a>
      </Link>
    </li>
    <li className="nav-item">
      <Link href="/r" as="/r">
        <a className="nav-link">Reference Sheets</a>
      </Link>
    </li>
  </ul>
);

export default Header;
