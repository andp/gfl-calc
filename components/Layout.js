import React, { Fragment } from 'react';
import Head from 'next/head';
import Header from './Header';

const Layout = props => (
  <Fragment>
    <Head>
      <title>416&apos;s Grind Hellhole</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
        integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
        crossOrigin="anonymous"
        global="true"
      />
    </Head>
    <div className="container">
      <Header />
      {props.children}
    </div>
    <img src="http://im.jezza.moe/416.jpg" id="i416" />
    <style jsx global>
      {`
        body {
          padding: 50px;
          font: 14px 'Lucida Grande', Helvetica, Arial, sans-serif;
        }

        a {
          color: #00b7ff;
        }
        #i416 {
          position: fixed;
          bottom: -16px;
          right: -20px;
        }
        #form {
          margin-bottom: 20px;
        }
        #results {
          margin-top: 20px;
        }
        #countdown {
          margin-left: 20px;
        }
        .container {
          position: relative;
          background: #fff;
          z-index: 1;
          padding-bottom: 5px;
        }
        .nav {
          margin-bottom: 10px;
        }
        @media (max-width: 640px) {
          body {
            padding: 0;
          }
          .nav li {
            width: 100%;
          }
        }
      `}
    </style>
  </Fragment>
);

export default Layout;
