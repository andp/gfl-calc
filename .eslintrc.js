module.exports = {
  extends: 'airbnb-base',
  env: {
    es6: true,
    browser: true,
    node: true,
    jest: true
  },
  rules: {
    'one-var': 'off',
    'comma-dangle': ['error', 'never'],
    'react/jsx-no-undef': 1,
    'react/jsx-sort-props': 0,
    'react/jsx-uses-react': 1,
    'react/jsx-uses-vars': 1,
    'react/jsx-wrap-multilines': 1,
    'react/no-did-mount-set-state': 0,
    'react/no-did-update-set-state': 1,
    'react/no-deprecated': 1,
    'react/no-unescaped-entities': 1,
    'react/no-multi-comp': 0,
    'react/no-unknown-property': 1,
    'react/no-unused-prop-types': 1,
    'react/self-closing-comp': 1,
    'react/default-props-match-prop-types': [1, { allowRequiredDefaults: true }],
    'react/sort-comp': 1,
    'class-methods-use-this': 0,
    'arrow-parens': ['error', 'as-needed']
  },
  parser: 'babel-eslint',
  plugins: ['react'],
  ecmaFeatures: { jsx: true }
};
