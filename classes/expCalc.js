const { cumExpArr, expArr } = require('../data/expData');

/*
 * Leader bonus is applied then MVP
 */
const leaderBonus = 1.2;
const mvpBonus = 1.3;
const eventBonus = 1.5;
/*
  1 - 10 at 1x,
  10 - 30 at 1.5x,
  30 - 70 at 2x,
  70 - 90 at 2.5x,
  90 - 100 at 3x
*/
const linkExpBonus = [1, 1.5, 2, 2.5, 3];
const coreIncLevels = [1, 10, 30, 70, 90];
const stages = {
  four3e: {
    base: 370,
    maxLevel: 75,
    amountOfBattles: 4,
    displayName: '4-3e'
  },
  five2e: {
    base: 410,
    maxLevel: 87,
    amountOfBattles: 5,
    displayName: '5-2e'
  },
  zero2: {
    base: 490,
    maxLevel: 100,
    amountOfBattles: 5,
    displayName: '0-2'
  }
};

class ExpCalc {
  constructor({
    initialLevel = 1,
    desiredLevel = 74,
    currentExp = 0,
    isMvp = false,
    isLeader = false,
    isEvent = false,
    stage
  }) {
    this.data = {
      initialLevel,
      desiredLevel,
      currentExp,
      isMvp,
      isLeader,
      isEvent,
      stage
    };
  }

  getInitialCoreMult() {
    return linkExpBonus[coreIncLevels.filter(level => level <= this.data.initialLevel).length - 1];
  }

  getExpDifference() {
    const { desiredLevel, initialLevel, currentExp } = this.data,
      desiredExpAmount = cumExpArr[desiredLevel - 1],
      initialExpAmount = cumExpArr[initialLevel - 1] + currentExp,
      amountOfLevels = desiredLevel - initialLevel,
      expDifference = expArr
        .slice(initialLevel - 1, desiredLevel - 1)
        .reduce((totalExp, currExp) => totalExp + currExp) - currentExp;
    return Object.assign(this.data, {
      desiredExpAmount,
      initialExpAmount,
      expDifference,
      amountOfLevels
    });
  }

  calculateCombatReports() {
    // this.getExpDifference();
    const { expDifference, currentExp } = this.data;
    const differenceAfterCurrent = expDifference - currentExp;
    const amountOfCombatReports = Math.ceil(differenceAfterCurrent / 3000);
    return Object.assign(this.data, {
      amountOfCombatReports
    });
  }

  calculateRuns() {
    this.getExpDifference();
    this.calculateCombatReports();
    const {
        expDifference, isMvp, isLeader, isEvent, stage
      } = this.data,
      { base, amountOfBattles, displayName } = stages[stage],
      coreBonus = this.getInitialCoreMult();

    let expPerBattle = base;
    // Handle leader bonus
    if (isLeader) {
      expPerBattle *= leaderBonus;
    }
    // Handle MVP bonus
    if (isMvp && isLeader) {
      expPerBattle *= mvpBonus;
    }
    // Handle Exp event bonus
    if (isEvent) {
      expPerBattle *= eventBonus;
    }
    // Multiply by amount of links
    expPerBattle *= coreBonus;

    const totalBattles = Math.ceil(expDifference / expPerBattle);
    const totalExpPerRun = expPerBattle * amountOfBattles;
    const runs = Math.ceil(expDifference / totalExpPerRun);
    return Object.assign(this.data, {
      coreBonus,
      expPerBattle,
      displayName,
      totalExpPerRun,
      totalBattles,
      runs
    });
  }
}

module.exports = ExpCalc;
