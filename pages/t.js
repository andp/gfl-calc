import { Fragment } from 'react';
import Layout from '../components/Layout';
import data from '../data/dollData';
import { doll } from '../data/recipeData';
import Table from '../components/layout/Table';
import TableHeader from '../components/layout/TableHeader';

export default () => (
  <Layout>
    <Table headerText="T-Doll Crafting" subtext="No voodoo recipes included">
      <Fragment>
        <TableHeader headerText="Doll Type" />
        <tbody>
          {Object.entries(doll).map(([key, value]) => (
            <tr key={key}>
              <th scope="row">{key}</th>
              <td>{value.manpower}</td>
              <td>{value.ammo}</td>
              <td>{value.rations}</td>
              <td>{value.parts}</td>
            </tr>
          ))}
        </tbody>
      </Fragment>
    </Table>
    <Table headerText="T-Doll Times" subtext="Time it takes for each doll to be produced">
      <Fragment>
        <TableHeader columnNames={['Craft Time', 'Rarity', 'Class', 'Dolls']} />
        <tbody>
          {data.map(item => (
            <tr key={item.dolls}>
              <td>{item.time}</td>
              <td>{'★'.repeat(item.rarity)}</td>
              <td>{item.class}</td>
              <td>{item.dolls}</td>
            </tr>
          ))}
        </tbody>
      </Fragment>
    </Table>
  </Layout>
);
