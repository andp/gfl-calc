import React, { Fragment } from 'react';
import Layout from '../components/Layout';
import equipmentData from '../data/equipData';
import { fairy } from '../data/recipeData';
import Table from '../components/layout/Table';
import TableHeader from '../components/layout/TableHeader';

export default () => (
  <Layout>
    <Table headerText="Equipment Crafting" subtext="Now with voodoo recipes">
      <Fragment>
        <TableHeader headerText="Equipment Recipe" />
        <tbody>
          <tr>
            <th scope="row">Optics:</th>
            <td>10</td>
            <td>10</td>
            <td>10</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>230</td>
            <td>80</td>
            <td>150</td>
            <td>80</td>
          </tr>
          <tr>
            <th scope="row">PEQ Laser Target Pointers:</th>
            <td>100</td>
            <td>10</td>
            <td>100</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>210</td>
            <td>50</td>
            <td>210</td>
            <td>210</td>
          </tr>
          <tr>
            <th scope="row">Suppressor:</th>
            <td>50</td>
            <td>10</td>
            <td>10</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>210</td>
            <td>50</td>
            <td>210</td>
            <td>210</td>
          </tr>
          <tr>
            <th scope="row">Ammunitions:</th>
            <td>10</td>
            <td>151</td>
            <td>10</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>50</td>
            <td>250</td>
            <td>150</td>
            <td>100</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>210</td>
            <td>210</td>
            <td>10</td>
            <td>110</td>
          </tr>
          <tr>
            <th scope="row">Exoskeletons:</th>
            <td>50</td>
            <td>10</td>
            <td>10</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>50</td>
            <td>50</td>
            <td>50</td>
            <td>210</td>
          </tr>
          <tr>
            <th scope="row">Body Armor:</th>
            <td>10</td>
            <td>10</td>
            <td>50</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>50</td>
            <td>50</td>
            <td>50</td>
            <td>210</td>
          </tr>
          <tr>
            <th scope="row">Extended Ammo Belt (MG Only):</th>
            <td>10</td>
            <td>10</td>
            <td>10</td>
            <td>151</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>50</td>
            <td>50</td>
            <td>50</td>
            <td>210</td>
          </tr>
          <tr>
            <th scope="row">Camoflauge Cape:</th>
            <td>100</td>
            <td>10</td>
            <td>100</td>
            <td>10</td>
          </tr>
          <tr>
            <td>Voodoo</td>
            <td>210</td>
            <td>50</td>
            <td>210</td>
            <td>210</td>
          </tr>
          <tr>
            <th scope="row">Universal:</th>
            <td> 150</td>
            <td> 150</td>
            <td> 150</td>
            <td> 150</td>
          </tr>
          <tr>
            <th scope="row">Universal (without optics):</th>
            <td>151</td>
            <td>151</td>
            <td>151</td>
            <td>151</td>
          </tr>
          <tr>
            <th scope="row">Universal (without ammunitions):</th>
            <td>110</td>
            <td>110</td>
            <td>110</td>
            <td>150</td>
          </tr>
        </tbody>
      </Fragment>
    </Table>
    <Table headerText="Fairy Recipes">
      <Fragment>
        <TableHeader headerText="Fairy Recipe" />
        <tbody>
          {Object.entries(fairy).map(([key, value]) => (
            <tr key={key}>
              <th scope="row">{key}</th>
              <td>{value.manpower}</td>
              <td>{value.ammo}</td>
              <td>{value.rations}</td>
              <td>{value.parts}</td>
            </tr>
          ))}
        </tbody>
      </Fragment>
    </Table>
    {equipmentData.map(equipmentType => (
      <Table
        key={equipmentType.header}
        headerText={equipmentType.header}
        subtext={equipmentType.subtext}
      >
        <Fragment>
          <TableHeader columnNames={equipmentType.columnNames} />
          <tbody>
            {equipmentType.data.map(item => (
              <tr key={item.name + item.rarity}>
                <td>{item.time}</td>
                {item.rarity && <td>{'★'.repeat(item.rarity)}</td>}
                <td>{item.type}</td>
                <td>{item.name}</td>
              </tr>
            ))}
          </tbody>
        </Fragment>
      </Table>
    ))}
  </Layout>
);
