import React, { Fragment } from 'react';
import Layout from '../components/Layout';
import skillData from '../data/skillData';
import Table from '../components/layout/Table';
import TableHeader from '../components/layout/TableHeader';

export default () => (
  <Layout>
    {skillData.map(dataType => (
      <Fragment key={dataType.header}>
        <Table headerText={dataType.header} subtext={dataType.body}>
          <Fragment>
            <TableHeader columnNames={['Level', 'Training Data', 'Time']} />
            <tbody>
              {dataType.data.map(item => (
                <tr key={item.level + item.amount}>
                  <td>
                    {item.level} &rarr; {item.level + 1}
                  </td>
                  <td>
                    {item.amount}x {item.type}
                  </td>
                  <td>{item.time}h</td>
                </tr>
              ))}
            </tbody>
          </Fragment>
        </Table>
      </Fragment>
    ))}
  </Layout>
);
