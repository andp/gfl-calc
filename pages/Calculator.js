import React, { useState } from 'react';
import Layout from '../components/Layout';
import ExpCalc from '../classes/expCalc';
import Countdown from '../components/Calculator/Countdown';
import ExpTable from '../components/Calculator/ExpTable';
import { localGet, localSet } from '../utils/storage';
import isObjectEmpty from '../utils/object';

export default function Calculator() {
  const initialData = Object.assign({}, localGet('formData') || {});

  const [formData, setFormData] = useState(initialData);
  const [showCountdown, setShowCountdown] = useState(!isObjectEmpty(initialData));

  const handleSubmit = e => {
    e.preventDefault();
    const formSubmitData = [...new FormData(e.target)].reduce(
      (obj, data) => Object.assign(obj, {
        [data[0]]: data[1] || null
      }),
      {}
    );
    const data = new ExpCalc(formSubmitData).calculateRuns();
    localSet('formData', data);
    setFormData(data);
    setShowCountdown(true);
  };

  return (
    <Layout>
      <h1>Exp Calc</h1>
      <p>I think this works regularly now</p>
      <form id="form" onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-group col-md-3 col-sm-12">
            <label htmlFor="initialLevel">Initial Level</label>
            <input
              className="form-control"
              id="initialLevel"
              type="number"
              name="initialLevel"
              defaultValue="30"
              placeholder="Initial Level"
            />
          </div>
          <div className="form-group col-md-3 col-sm-12">
            <label htmlFor="desiredLevel">Desired Level</label>
            <input
              className="form-control"
              id="desiredLevel"
              type="number"
              name="desiredLevel"
              defaultValue="74"
              placeholder="Desired Level"
            />
          </div>
          <div className="form-group col-md-3 col-sm-12">
            <label htmlFor="currentExp">Current Exp</label>
            <input
              className="form-control"
              id="currentExp"
              type="number"
              name="currentExp"
              placeholder="Current Exp"
            />
          </div>
          <div className="form-group col-md-3 col-sm-12">
            <label htmlFor="stage">Stage Select</label>
            <select className="form-control" name="stage" id="stage">
              <option value="four3e">4-3e</option>
              <option value="five2e">5-2e</option>
              <option value="zero2">0-2</option>
            </select>
          </div>
        </div>
        <div className="form-group">
          <div className="form-check form-check-inline">
            <input className="form-check-input" id="isLeader" type="checkbox" name="isLeader" />
            <label className="form-check-label" htmlFor="isLeader">
              Unit is Leader
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" id="isMvp" type="checkbox" name="isMvp" />
            <label className="form-check-label" htmlFor="isMvp">
              Unit is MVP
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" id="isEvent" type="checkbox" name="isEvent" />
            <label className="form-check-label" htmlFor="isEvent">
              Exp Event is Happening
            </label>
          </div>
        </div>
        <button className="btn btn-primary" type="submit">
          Calculate
        </button>
        {showCountdown && <Countdown number={formData.runs} />}
      </form>
      {formData.runs && <ExpTable {...formData} />}
    </Layout>
  );
}
