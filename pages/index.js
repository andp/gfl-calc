import React, { Fragment } from 'react';
import Calculator from './Calculator';

export default () => (
  <Fragment>
    <Calculator />
  </Fragment>
);
