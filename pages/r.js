import { Fragment } from 'react';
import Layout from '../components/Layout';
import data from '../data/referenceData';

export default () => (
  <Layout>
    <Fragment>
      <h1>Reference Links</h1>
      <ul>
        {data.map(item => (
          <li key={item.linkText}>
            <a href={item.link} target="_blank">
              {item.linkText}
            </a>
          </li>
        ))}
      </ul>
    </Fragment>
  </Layout>
);
