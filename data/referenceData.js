module.exports = [
  {
    linkText: "Costume spreadsheet",
    link:
      "https://docs.google.com/spreadsheets/d/181WdcBjuMKmV-KSGzNSJaOV9mq4gqgaQ-Pmamw8RXtE/edit#gid=1295025188"
  },
  {
    linkText: "Maps",
    link: "https://gf.underseaworld.net/en/maps"
  },
  {
    linkText: "General gameplay timers",
    link: "https://gftimers.netlify.com/"
  },
  {
    linkText:
      "Spoonfeeder site, general gameplay and mechanics breakdowns and handhold guides",
    link: "https://www.gflcorner.com/"
  },
  {
    linkText: "Voodoo recipes and craft time",
    link: "https://sangvis.science/"
  },
  {
    linkText: "Wiki (patchy data and runs like shit)",
    link: "https://en.gfwiki.com/"
  },
  {
    linkText: "Equipment quick reference",
    link: "https://ro635.com/megaphone/Equipment_Crafting_Chart.jpg"
  },
  {
    linkText: "Waifu lookup site, some additional functions",
    link: "https://gfl.zzzzz.kr/"
  },
  {
    linkText: "Digimind investment",
    link:
      "https://media.discordapp.net/attachments/629378508761595904/629445184454918154/Screenshot_at_2019-09-25_14-33-44.png"
  },
  {
    linkText: "Zas drag resource (link to 8-1N on sheet)",
    link:
      "https://docs.google.com/spreadsheets/d/1at4xE_8hIdFatxoDIzIeyAHg0BMHk66HLYR7Ei3aBKU/edit#gid=0"
  },
  {
    linkText: "Fairy info",
    link:
      "https://docs.google.com/spreadsheets/d/1x6_YysDi0h89jKE9vEW2_fbxi7gG7XV5jjJqX8O41rw/"
  },
  {
    linkText: "10-4e dragon guide",
    link: "https://www.youtube.com/watch?v=ok1I9UvYO-Y"
  }
];
