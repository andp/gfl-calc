module.exports = [
  {
    header: 'T-Doll Skill Training',
    body: '600x Beginner, 1020x Intermediate, 500x Advanced from 1 → 10',
    data: [
      {
        level: 1,
        type: 'Beginner',
        amount: '100',
        time: '1'
      },
      {
        level: 2,
        type: 'Beginner',
        amount: '200',
        time: '2'
      },
      {
        level: 3,
        type: 'Beginner',
        amount: '300',
        time: '3'
      },
      {
        level: 4,
        type: 'Intermediate',
        amount: '120',
        time: '4'
      },
      {
        level: 5,
        type: 'Intermediate',
        amount: '200',
        time: '6'
      },
      {
        level: 6,
        type: 'Intermediate',
        amount: '300',
        time: '9'
      },
      {
        level: 7,
        type: 'Intermediate',
        amount: '400',
        time: '12'
      },
      {
        level: 8,
        type: 'Advanced',
        amount: '200',
        time: '18'
      },
      {
        level: 9,
        type: 'Advanced',
        amount: '300',
        time: '24'
      }
    ]
  },
  {
    header: 'Combat Skill Training',
    body: '1200x Beginner, 1200 Intermediate, 1200x Advanced from 1 → 10',
    data: [
      {
        level: 1,
        type: 'Beginner',
        amount: '200',
        time: '2'
      },
      {
        level: 2,
        type: 'Beginner',
        amount: '400',
        time: '6'
      },
      {
        level: 3,
        type: 'Beginner',
        amount: '600',
        time: '10'
      },
      {
        level: 4,
        type: 'Intermediate',
        amount: '200',
        time: '14'
      },
      {
        level: 5,
        type: 'Intermediate',
        amount: '400',
        time: '20'
      },
      {
        level: 6,
        type: 'Intermediate',
        amount: '600',
        time: '26'
      },
      {
        level: 7,
        type: 'Advanced',
        amount: '200',
        time: '32'
      },
      {
        level: 8,
        type: 'Advanced',
        amount: '400',
        time: '40'
      },
      {
        level: 9,
        type: 'Advanced',
        amount: '600',
        time: '48'
      }
    ]
  },
  {
    header: 'Strategy Skill Training',
    body: '1200x Beginner, 2040x Intermediate, 1000x Advanced from 1 → 10',
    data: [
      {
        level: 1,
        type: 'Beginner',
        amount: '200',
        time: '2'
      },
      {
        level: 2,
        type: 'Beginner',
        amount: '400',
        time: '6'
      },
      {
        level: 3,
        type: 'Beginner',
        amount: '600',
        time: '10'
      },
      {
        level: 4,
        type: 'Intermediate',
        amount: '240',
        time: '14'
      },
      {
        level: 5,
        type: 'Intermediate',
        amount: '400',
        time: '20'
      },
      {
        level: 6,
        type: 'Intermediate',
        amount: '600',
        time: '26'
      },
      {
        level: 7,
        type: 'Intermediate',
        amount: '800',
        time: '32'
      },
      {
        level: 8,
        type: 'Advanced',
        amount: '400',
        time: '40'
      },
      {
        level: 9,
        type: 'Advanced',
        amount: '600',
        time: '48'
      }
    ]
  }
];
