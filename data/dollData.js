module.exports = [
  {
    time: '0:20',
    rarity: 2,
    class: 'HG',
    dolls: 'M1911, Nagant Revolver, P38'
  },
  {
    time: '0:22',
    rarity: 2,
    class: 'HG',
    dolls: 'PPK'
  },
  {
    time: '0:25',
    rarity: 2,
    class: 'HG',
    dolls: 'FNP-9, MP-446'
  },
  {
    time: '0:28',
    rarity: 2,
    class: 'HG',
    dolls: 'USP Compact, Bren Ten'
  },
  {
    time: '0:30',
    rarity: 3,
    class: 'HG',
    dolls: 'P08, C96'
  },
  {
    time: '0:35',
    rarity: 3,
    class: 'HG',
    dolls: 'Type 92, P99'
  },
  {
    time: '0:40',
    rarity: 3,
    class: 'HG',
    dolls: 'Astra Revolver, M9, Makarov'
  },
  {
    time: '0:45',
    rarity: 3,
    class: 'HG',
    dolls: 'Tokarev'
  },
  {
    time: '0:49',
    rarity: 4,
    class: 'HG',
    dolls: 'Jericho'
  },
  {
    time: '0:50',
    rarity: 4,
    class: 'HG',
    dolls: 'Colt SAA, Mk23'
  },
  {
    time: '0:52',
    rarity: 4,
    class: 'HG',
    dolls: 'Spitfire'
  },
  {
    time: '0:53',
    rarity: 4,
    class: 'HG',
    dolls: 'K5'
  },
  {
    time: '0:55',
    rarity: 4,
    class: 'HG',
    dolls: 'P7, Stechkin'
  },
  {
    time: '1:00',
    rarity: 5,
    class: 'HG',
    dolls: 'Welrod Mk II'
  },
  {
    time: '1:02',
    rarity: 5,
    class: 'HG',
    dolls: 'Contender'
  },
  {
    time: '1:05',
    rarity: 5,
    class: 'HG',
    dolls: 'M950A, NZ75'
  },
  {
    time: '1:10',
    rarity: 5,
    class: 'HG',
    dolls: 'Grizzly Mk V'
  },
  {
    time: '1:10',
    rarity: 2,
    class: 'SMG',
    dolls: 'IDW, PP-2000'
  },
  {
    time: '1:20',
    rarity: 2,
    class: 'SMG',
    dolls: 'Spectre M4, M45'
  },
  {
    time: '1:25',
    rarity: 2,
    class: 'SMG',
    dolls: 'Type 64'
  },
  {
    time: '1:30',
    rarity: 2,
    class: 'SMG',
    dolls: 'MP40, Beretta 38, M3'
  },
  {
    time: '1:40',
    rarity: 3,
    class: 'SMG',
    dolls: 'Sten Mk II, Micro Uzi'
  },
  {
    time: '1:50',
    rarity: 3,
    class: 'SMG',
    dolls: 'F1 [Heavy]'
  },
  {
    time: '1:50',
    rarity: 2,
    class: 'SMG',
    dolls: 'PPSh-41'
  },
  {
    time: '2:00',
    rarity: 3,
    class: 'SMG',
    dolls: 'MAC-10, Skorpion'
  },
  {
    time: '2:05',
    rarity: 3,
    class: 'SMG',
    dolls: 'Z-62 [Heavy]'
  },
  {
    time: '2:10',
    rarity: 3,
    class: 'SMG',
    dolls: 'PPS-43'
  },
  {
    time: '2:15',
    rarity: 4,
    class: 'SMG',
    dolls: 'UMP9, UMP45'
  },
  {
    time: '2:18',
    rarity: 4,
    class: 'SMG',
    dolls: 'Shipka, PP-19-01'
  },
  {
    time: '2:20',
    rarity: 4,
    class: 'SMG',
    dolls: 'MP5, PP-90'
  },
  {
    time: '2:25',
    rarity: 5,
    class: 'SMG',
    dolls: 'Suomi'
  },
  {
    time: '2:28',
    rarity: 5,
    class: 'SMG',
    dolls: 'C-MS'
  },
  {
    time: '2:30',
    rarity: 5,
    class: 'SMG',
    dolls: 'Thompson, G36C'
  },
  {
    time: '2:33',
    rarity: 5,
    class: 'SMG',
    dolls: 'SR-3MP'
  },
  {
    time: '2:35',
    rarity: 5,
    class: 'SMG',
    dolls: 'Vector, Type 79'
  },
  {
    time: '2:40',
    rarity: 2,
    class: 'AR',
    dolls: 'Galil, SIG-510'
  },
  {
    time: '2:45',
    rarity: 2,
    class: 'AR',
    dolls: 'F2000, Type 63'
  },
  {
    time: '2:50',
    rarity: 2,
    class: 'AR',
    dolls: 'L85A1, G3'
  },
  {
    time: '3:00',
    rarity: 3,
    class: 'AR',
    dolls: 'StG 44'
  },
  {
    time: '3:10',
    rarity: 3,
    class: 'AR',
    dolls: 'OTs-12'
  },
  {
    time: '3:10',
    rarity: 2,
    class: 'RF',
    dolls: 'G43, FN-49'
  },
  {
    time: '3:15',
    rarity: 3,
    class: 'AR',
    dolls: 'ARX-160 [Heavy]'
  },
  {
    time: '3:20',
    rarity: 3,
    class: 'AR',
    dolls: 'AK-47, FNC'
  },
  {
    time: '3:20',
    rarity: 2,
    class: 'RF',
    dolls: 'BM59'
  },
  {
    time: '3:25',
    rarity: 4,
    class: 'AR',
    dolls: 'Type 56-1, XM8'
  },
  {
    time: '3:30',
    rarity: 4,
    class: 'AR',
    dolls: 'AS Val, FAMAS, TAR-21'
  },
  {
    time: '3:30',
    rarity: 2,
    class: 'RF',
    dolls: 'SKS, SVT-38'
  },
  {
    time: '3:35',
    rarity: 4,
    class: 'AR',
    dolls: '9A-91'
  },
  {
    time: '3:40',
    rarity: 4,
    class: 'AR',
    dolls: 'G36, Ribeyrolles'
  },
  {
    time: '3:40',
    rarity: 3,
    class: 'RF',
    dolls: 'M14, SV-98'
  },
  {
    time: '3:45',
    rarity: 5,
    class: 'AR',
    dolls: 'FAL'
  },
  {
    time: '3:48',
    rarity: 5,
    class: 'AR',
    dolls: 'T91'
  },
  {
    time: '3:50',
    rarity: 5,
    class: 'AR',
    dolls: 'Type 95, Type 97'
  },
  {
    time: '3:50',
    rarity: 3,
    class: 'RF',
    dolls: 'Hanyang Type 88, OTs-44 [Heavy]'
  },
  {
    time: '3:52',
    rarity: 5,
    class: 'AR',
    dolls: 'K2'
  },
  {
    time: '3:53',
    rarity: 5,
    class: 'AR',
    dolls: 'MDR'
  },
  {
    time: '3:55',
    rarity: 5,
    class: 'AR',
    dolls: 'HK416'
  },
  {
    time: '3:58',
    rarity: 5,
    class: 'AR',
    dolls: 'RFB'
  },
  {
    time: '4:00',
    rarity: 3,
    class: 'RF',
    dolls: 'M1 Garand'
  },
  {
    time: '4:04',
    rarity: 5,
    class: 'AR',
    dolls: 'G11'
  },
  {
    time: '4:05',
    rarity: 5,
    class: 'AR',
    dolls: 'G41, Zas M21'
  },
  {
    time: '4:09',
    rarity: 5,
    class: 'AR',
    dolls: 'AN-94'
  },
  {
    time: '4:10',
    rarity: 4,
    class: 'RF',
    dolls: 'Mosin Nagant, T-5000'
  },
  {
    time: '4:12',
    rarity: 5,
    class: 'AR',
    dolls: 'AK-12'
  },
  {
    time: '4:15',
    rarity: 4,
    class: 'RF',
    dolls: 'SVD'
  },
  {
    time: '4:20',
    rarity: 4,
    class: 'RF',
    dolls: 'PSG-1 [Heavy], G28 [Heavy]'
  },
  {
    time: '4:25',
    rarity: 4,
    class: 'RF',
    dolls: 'Springfield'
  },
  {
    time: '4:30',
    rarity: 4,
    class: 'RF',
    dolls: 'PTRD, PzB 39 [Heavy]'
  },
  {
    time: '4:38',
    rarity: 5,
    class: 'RF',
    dolls: 'Carcano M1891'
  },
  {
    time: '4:40',
    rarity: 5,
    class: 'RF',
    dolls: 'Kar 98k'
  },
  {
    time: '4:42',
    rarity: 5,
    class: 'RF',
    dolls: 'Carcano M91/38'
  },
  {
    time: '4:45',
    rarity: 5,
    class: 'RF',
    dolls: 'NTW-20'
  },
  {
    time: '4:50',
    rarity: 5,
    class: 'RF',
    dolls: 'WA2000'
  },
  {
    time: '4:50',
    rarity: 2,
    class: 'MG',
    dolls: 'AAT-52, FG42'
  },
  {
    time: '4:52',
    rarity: 5,
    class: 'RF',
    dolls: 'IWS 2000'
  },
  {
    time: '4:55',
    rarity: 5,
    class: 'RF',
    dolls: 'M99'
  },
  {
    time: '5:00',
    rarity: 5,
    class: 'RF',
    dolls: 'Lee Enfield'
  },
  {
    time: '5:00',
    rarity: 2,
    class: 'MG',
    dolls: 'MG34, DP28'
  },
  {
    time: '5:10',
    rarity: 2,
    class: 'MG',
    dolls: 'LWMMG'
  },
  {
    time: '5:20',
    rarity: 3,
    class: 'MG',
    dolls: 'Bren'
  },
  {
    time: '5:40',
    rarity: 3,
    class: 'MG',
    dolls: 'M1919A4'
  },
  {
    time: '5:50',
    rarity: 3,
    class: 'MG',
    dolls: 'MG42'
  },
  {
    time: '6:10',
    rarity: 3,
    class: 'MG',
    dolls: 'M2HB'
  },
  {
    time: '6:10',
    rarity: 4,
    class: 'MG',
    dolls: 'M60'
  },
  {
    time: '6:15',
    rarity: 4,
    class: 'MG',
    dolls: 'Type 80'
  },
  {
    time: '6:20',
    rarity: 4,
    class: 'MG',
    dolls: 'Mk48, AEK-999'
  },
  {
    time: '6:25',
    rarity: 4,
    class: 'MG',
    dolls: 'M1918 BAR, Ameli'
  },
  {
    time: '6:30',
    rarity: 4,
    class: 'MG',
    dolls: 'PK, MG3'
  },
  {
    time: '6:35',
    rarity: 5,
    class: 'MG',
    dolls: 'Negev'
  },
  {
    time: '6:40',
    rarity: 5,
    class: 'MG',
    dolls: 'MG4'
  },
  {
    time: '6:45',
    rarity: 5,
    class: 'MG',
    dolls: 'MG5'
  },
  {
    time: '6:50',
    rarity: 5,
    class: 'MG',
    dolls: 'PKP'
  },
  {
    time: '7:14',
    rarity: 4,
    class: 'SG',
    dolls: 'M1014'
  },
  {
    time: '7:15',
    rarity: 3,
    class: 'SG',
    dolls: 'NS2000'
  },
  {
    time: '7:20',
    rarity: 3,
    class: 'SG',
    dolls: 'M500'
  },
  {
    time: '7:25',
    rarity: 3,
    class: 'SG',
    dolls: 'KS-23'
  },
  {
    time: '7:30',
    rarity: 3,
    class: 'SG',
    dolls: 'RMB-93, M1897'
  },
  {
    time: '7:40',
    rarity: 4,
    class: 'SG',
    dolls: 'M590, SPAS-12'
  },
  {
    time: '7:45',
    rarity: 4,
    class: 'SG',
    dolls: 'M37'
  },
  {
    time: '7:50',
    rarity: 4,
    class: 'SG',
    dolls: 'Super-Shorty'
  },
  {
    time: '7:55',
    rarity: 4,
    class: 'SG',
    dolls: 'USAS-12'
  },
  {
    time: '8:00',
    rarity: 5,
    class: 'SG',
    dolls: 'KSG'
  },
  {
    time: '8:05',
    rarity: 5,
    class: 'SG',
    dolls: 'Saiga-12'
  },
  {
    time: '8:10',
    rarity: 5,
    class: 'SG',
    dolls: 'S.A.T.8'
  }
];
