module.exports = {
  fairy: {
    'Combat/Illuminate/General': {
      manpower: 500,
      ammo: 500,
      rations: 500,
      parts: 500
    },
    'Reinforcement/Parachute/Defense': {
      manpower: 2000,
      ammo: 500,
      rations: 2000,
      parts: 1000
    },
    'Landmine/Rocket/Construction': {
      manpower: 500,
      ammo: 2000,
      rations: 2000,
      parts: 1000
    }
  },
  equipment: {},
  doll: {
    'HG (buffer):': {
      manpower: 130,
      ammo: 130,
      rations: 130,
      parts: 30
    },
    'SMG (tank):': {
      manpower: 400,
      ammo: 400,
      rations: 91,
      parts: 30
    },
    'AR (DPS):': {
      manpower: 91,
      ammo: 400,
      rations: 400,
      parts: 30
    },
    'RF (DPS):': {
      manpower: 400,
      ammo: 91,
      rations: 400,
      parts: 30
    },
    'MG (DPS):': {
      manpower: 600,
      ammo: 600,
      rations: 100,
      parts: 400
    },
    'Semi-Global (all but HG and MG):': {
      manpower: 400,
      ammo: 400,
      rations: 400,
      parts: 30
    },
    'Semi-Global (all but HG):': {
      manpower: 600,
      ammo: 600,
      rations: 400,
      parts: 400
    },
    'SG (tank):': {
      manpower: 6000,
      ammo: 2000,
      rations: 6000,
      parts: 4000
    }
  }
};
