module.exports = [
  {
    header: 'Equipment Times',
    subtext: 'Time it takes for each equip to be produced',
    columnNames: ['Craft Time', 'Rarity', 'Type', 'Equipment Name'],
    data: [
      {
        time: '0:50',
        rarity: 2,
        type: 'Telescope',
        name: 'BM 3-12X40'
      },
      {
        time: '0:70',
        rarity: 2,
        type: 'Suppressor',
        name: 'AC1'
      },
      {
        time: '0:80',
        rarity: 2,
        type: 'EOTech',
        name: 'EOT 506'
      },
      {
        time: '0:90',
        rarity: 2,
        type: 'PEQ',
        name: 'PEQ-2'
      },
      {
        time: '0:10',
        rarity: 2,
        type: 'Red Dot',
        name: 'AMP COMPM2'
      },
      {
        time: '0:12',
        rarity: 2,
        type: 'T Exo',
        name: 'IOP T1'
      },
      {
        time: '0:12',
        rarity: 2,
        type: 'X Exo',
        name: 'IOP X1'
      },
      {
        time: '0:13',
        rarity: 2,
        type: 'HP',
        name: 'ILM HP'
      },
      {
        time: '0:14',
        rarity: 2,
        type: 'Slug',
        name: 'BK'
      },
      {
        time: '0:14',
        rarity: 2,
        type: 'Buckshot',
        name: '#1 Buckshot'
      },
      {
        time: '0:15',
        rarity: 2,
        type: 'AP',
        name: 'M61'
      },
      {
        time: '0:16',
        rarity: 2,
        type: 'Cloak',
        name: 'Old Camouflage Cape'
      },
      {
        time: '0:18',
        rarity: 2,
        type: 'HV',
        name: 'FMJ'
      },
      {
        time: '0:20',
        rarity: 3,
        type: 'Telescope',
        name: 'LRA 2-12X50'
      },
      {
        time: '0:22',
        rarity: 3,
        type: 'Suppressor',
        name: 'AC2'
      },
      {
        time: '0:23',
        rarity: 3,
        type: 'EOTech',
        name: 'EOT 512'
      },
      {
        time: '0:24',
        rarity: 3,
        type: 'PEQ',
        name: 'PEQ-5'
      },
      {
        time: '0:25',
        rarity: 3,
        type: 'Red Dot',
        name: 'AMP COMPM4'
      },
      {
        time: '0:26',
        rarity: 3,
        type: 'Body Armor',
        name: 'type 1 Armor'
      },
      {
        time: '0:27',
        rarity: 3,
        type: 'T Exo',
        name: 'IOP T2'
      },
      {
        time: '0:27',
        rarity: 3,
        type: 'X Exo',
        name: 'IOP X2'
      },
      {
        time: '0:28',
        rarity: 3,
        type: 'HP',
        name: 'ILM HP'
      },
      {
        time: '0:29',
        rarity: 3,
        type: 'Slug',
        name: 'FST'
      },
      {
        time: '0:29',
        rarity: 3,
        type: 'Buckshot',
        name: '#0 Buckshot'
      },
      {
        time: '0:30',
        rarity: 3,
        type: 'AP',
        name: 'M993'
      },
      {
        time: '0:31',
        rarity: 3,
        type: 'Cloak',
        name: 'Camouflage Cape'
      },
      {
        time: '0:33',
        rarity: 3,
        type: 'HV',
        name: 'JSP'
      },
      {
        time: '0:35',
        rarity: 4,
        type: 'Telescope',
        name: 'PSO-1'
      },
      {
        time: '0:37',
        rarity: 4,
        type: 'Suppressor',
        name: 'AC3'
      },
      {
        time: '0:38',
        rarity: 4,
        type: 'EOTech',
        name: 'EOT 516'
      },
      {
        time: '0:39',
        rarity: 4,
        type: 'PEQ',
        name: 'PEQ-15'
      },
      {
        time: '0:40',
        rarity: 4,
        type: 'Red Dot',
        name: 'COG M150'
      },
      {
        time: '0:41',
        rarity: 4,
        type: 'Body Armor',
        name: 'type 2 Armor'
      },
      {
        time: '0:42',
        rarity: 4,
        type: 'T Exo',
        name: 'IOP T3'
      },
      {
        time: '0:42',
        rarity: 4,
        type: 'X Exo',
        name: 'IOP X3'
      },
      {
        time: '0:43',
        rarity: 4,
        type: 'HP',
        name: 'ILM HP'
      },
      {
        time: '0:44',
        rarity: 4,
        type: 'Slug',
        name: 'WAD'
      },
      {
        time: '0:44',
        rarity: 4,
        type: 'Buckshot',
        name: '#00 Buckshot'
      },
      {
        time: '0:45',
        rarity: 4,
        type: 'AP',
        name: 'MK169'
      },
      {
        time: '0:45',
        rarity: 5,
        type: 'Telescope',
        name: 'VFL 6-24X56'
      },
      {
        time: '0:46',
        rarity: 4,
        type: 'Cloak',
        name: 'Urban Camouflage'
      },
      {
        time: '0:47',
        rarity: 5,
        type: 'Suppressor',
        name: 'AC4'
      },
      {
        time: '0:47',
        rarity: 4,
        type: 'Ammo Belt',
        name: 'IOP Extended Ammo Belt'
      },
      {
        time: '0:48',
        rarity: 4,
        type: 'HV',
        name: 'JHP'
      },
      {
        time: '0:48',
        rarity: 5,
        type: 'EOTech',
        name: 'EOT 518'
      },
      {
        time: '0:49',
        rarity: 5,
        type: 'PEQ',
        name: 'PEQ-16A'
      },
      {
        time: '0:50',
        rarity: 5,
        type: 'Red Dot',
        name: 'ITI MARS'
      },
      {
        time: '0:51',
        rarity: 5,
        type: 'Body Armor',
        name: 'type 3 Armor'
      },
      {
        time: '0:52',
        rarity: 5,
        type: 'T Exo',
        name: 'IOP T4'
      },
      {
        time: '0:52',
        rarity: 5,
        type: 'X Exo',
        name: 'IOP X4'
      },
      {
        time: '0:53',
        rarity: 5,
        type: 'HP',
        name: 'ILM HP'
      },
      {
        time: '0:54',
        rarity: 5,
        type: 'Slug',
        name: 'SABOT'
      },
      {
        time: '0:54',
        rarity: 5,
        type: 'Buckshot',
        name: '#000 Buckshot'
      },
      {
        time: '0:55',
        rarity: 5,
        type: 'AP',
        name: 'Mk211 APHE'
      },
      {
        time: '0:56',
        rarity: 5,
        type: 'Cloak',
        name: 'Thermo-Optic Camo'
      },
      {
        time: '0:57',
        rarity: 5,
        type: 'Ammo Belt',
        name: 'IOP Extended Ammo Box'
      },
      {
        time: '0:58',
        rarity: 5,
        type: 'HV',
        name: 'APCR'
      }
    ]
  },
  {
    header: 'Fairy Times',
    subtext: 'Fairy/Equip Heavy Prod timers',
    columnNames: ['Craft Time', 'Type', 'Equipment Name'],
    data: [
      {
        time: '3:00',
        type: 'Combat Fairy',
        name: 'Armour'
      },
      {
        time: '3:05',
        type: 'Combat Fairy',
        name: 'Shield'
      },
      {
        time: '3:10',
        type: 'Combat Fairy',
        name: 'Taunt'
      },
      {
        time: '3:30',
        type: 'Combat Fairy',
        name: 'Sniper'
      },
      {
        time: '3:35',

        type: 'Combat Fairy',
        name: 'Artillery'
      },
      {
        time: '3:40',
        type: 'Combat Fairy',
        name: 'Airstrike'
      },
      {
        time: '4:30',
        type: 'Combat Fairy',
        name: 'Warrior'
      },
      {
        time: '4:35',
        type: 'Combat Fairy',
        name: 'Fury'
      },
      {
        time: '5:00',
        type: 'Combat Fairy',
        name: 'Command'
      },
      {
        time: '5:05',
        type: 'Combat Fairy',
        name: 'Rescue'
      },
      {
        time: '4:00',
        type: 'Strategy Fairy',
        name: 'Reinforcement'
      },
      {
        time: '4:05',
        type: 'Strategy Fairy',
        name: 'Parachute'
      },
      {
        time: '4:10',
        type: 'Strategy Fairy',
        name: 'Defense'
      },
      {
        time: '5:10',
        type: 'Strategy Fairy',
        name: 'Illuminate'
      },
      {
        time: '5:30',
        type: 'Strategy Fairy',
        name: 'Landmine'
      },
      {
        time: '5:35',
        type: 'Strategy Fairy',
        name: 'Rocket'
      },
      {
        time: '5:40',
        type: 'Strategy Fairy',
        name: 'Construction'
      }
    ]
  }
];
